'use strict';

var apselect = angular.module('ap.select', ['ng', 'ui.router', 'pascalprecht.translate']);

apselect.directive('select2', function($rootScope, $parse, $log, $document, $http, $translate, $filter, $timeout) {
  return {
    restrict: 'E',
    scope: {
      ngModel: '=',
      ngChange: '&',
      trans: '@',
      disabled: '@'
    },
    templateUrl: 'bower_components/apselect/templates/select2.html',
    transclude: true,
    link: function($scope, element){
      $scope.filter = '';
      $scope.select = '';
      $scope.items = [];

      var filter = element.find('.select2remotefilter');
      var dropdown = element.find('.select2remote');
      var input = element.find('.select2focusinput').children('input');
      var elem = element.find('.select2transclude');

      var observer = new MutationObserver(function(mutations) {
        readOptions();
      });

      observer.observe(elem[0], {
        childList: true,
        subtree: true
      });

      $rootScope.$on('$translateChangeSuccess', function() {
        readOptions();
      });

      var setValue = function(){
        var val;

        if($scope.items.length > 0){
          if($scope.ngModel === undefined){
            val = '';
          }else{
            val = $scope.ngModel;
          }

          var sel = $filter('filter')($scope.items, {value: String(val)}, true)[0];
          if(sel !== undefined){
            $scope.select = sel.label;
            $timeout(function(){
              $scope.$apply();
            });

          }else{
            $scope.select = '';
            $scope.ngModel = '';
          }
        }else{
          $scope.select = '';
        }
      };

      var readOptions = function(){
        $scope.items = [];
        var optionfields = elem.children("option");
        var i = 0;

        if($scope.trans === 'true'){
          optionfields.each(function(){
            var tran = $(this).text();
            var val = $(this).val();
            $translate(tran).then(function(translation){
              $scope.items.push({value: val, label: translation});
              i++;
              if(i >= optionfields.length){
                setValue();
              }
            });
          });

        }else{
          optionfields.each(function(){
            $scope.items.push({value: $(this).val(), label: $(this).text()});
            i++;
            if(i >= optionfields.length){
              setValue();
            }
          });
        }
      };

      readOptions();

      input.on('focus', function(){
        $scope.click(true);
      });

      $scope.setSelected = function(index){
        $scope.selected = index;
      };

      var setList = function(){

        if($scope.items.length === 0){
          $scope.items.push({value: false, label: 'Keine Einträge'});
        }

        $scope.selected = 0;
        element.unbind('keydown');
        element.on('keydown', function(e) {
          if(e.which === 9){
            closeList();
          }else if (e.which === 13) {
            e.stopPropagation();
            var items = $filter('filter')($scope.items, $scope.filter);
            $scope.setNewValue(items[$scope.selected]);
            $scope.$apply();
          }else if(e.which === 38){
            if($scope.selected > 0){
              $scope.selected--;
              $scope.$apply();
            }
          }else if(e.which === 40){
            if(($scope.selected + 1) < $scope.items.length){
              $scope.selected++;
              $scope.$apply();
            }
          }
        });
      };

      var closeList = function(){
        element.unbind('keydown');
        dropdown.removeClass('open');
      };

      filter.bind('click', function(e) {
        e.stopPropagation();
      });

      $scope.click = function(trigger){
        if($scope.disabled !== true){
          if(dropdown.hasClass('open')){
            closeList();
          }else{
            closeList();
            $('.select2remote').removeClass('open');
            dropdown.addClass('open');
            filter.focus();
            setList();
            if(trigger){
              $scope.$apply();
            }
          }
        }
      };

      $document.bind('click', function(event) {
        if (!element[0].contains(event.target)) {
          closeList();
        }
      });

      $scope.setNewValue = function(item, event){
        if(event){
          event.stopPropagation();
        }
        if(item){
          $scope.ngModel = item.value;
          $scope.select = item.label;
          input.focus();
          if($scope.ngChange){
            $timeout(function(){
              $scope.ngChange();
            }, 0);
          }
        }
      };

      $scope.$watch('filter', function(){
        $scope.selected = 0;
      });

      $scope.$watch('ngModel', function(newVal){
        setValue();
      });
    }
  };
});

apselect.directive('select2remote', function($parse, $modal, $log, $document, $http, $stateParams, $timeout) {
  return {
    restrict: 'E',
    scope: {
      remoteConfig: '=',
      ngModel: '=',
      ngChange: '&'
    },
    templateUrl: 'bower_components/apselect/templates/select2remote.html',
    replace: true,
    transclude: true,
    link: function($scope, element){

      var filter = element.find('.select2remotefilter');
      var dropdown = element.find('.select2remote');
      var input = element.find('.select2remotetransclude').children('input');

      input.on('focus', function(){
        $scope.click(true);
      });

      $scope.filter = '';
      $scope.select = '';
      $scope.items = [];

      // CONFIG
      var options = {
        table: 'tests',
        action: 'tests',
        watch: false,
        id: '_id',
        list: 'text',
        select: 'text',
        fields: []
      };

      angular.extend(options, $scope.remoteConfig);

      $scope.selected = 0;

      $scope.setSelected = function(index){
        $scope.selected = index;
      };

      var setList = function(data){
        $scope.selected = 0;
        $scope.items = data;

        if($scope.items.length === 0){$scope.items.push(false);} // if no entries

        dropdown.addClass('open');

        element.unbind('keydown');
        element.on('keydown', function(e) {
          if(e.which === 9){
            closeList();
          }else if (e.which === 13) {
            e.stopPropagation();
            $scope.setNewValue($scope.items[$scope.selected]);
            $scope.$apply();
          }else if(e.which === 38){
            if($scope.selected > 0){
              $scope.selected--;
              $scope.$apply();
            }
          }else if(e.which === 40){
            if(($scope.selected + 1) < $scope.items.length){
              $scope.selected++;
              $scope.$apply();
            }
          }
        });

      };

      var closeList = function(){
        element.unbind('keydown');
        $scope.items = [];
        dropdown.removeClass('open');
      };

      element.bind('click', function(e) {
        e.stopPropagation();
      });

      filter.bind('click', function(e) {
        e.stopPropagation();
      });

      $scope.click = function(trigger){
        if(dropdown.hasClass('open')){
          closeList();
        }else{
          $('.select2remote').removeClass('open');
          dropdown.addClass('open');
          filter.focus();
          $scope.$watch('filter', function(newValue){
            $http.post('/search/select', {table: options.table, q: newValue, id: '', fields: options.fields}).then(function (res) {
              if(res.data){
                setList(res.data);
              }else{
                $scope.items = [];
              }
            });
          });
          if(trigger){
            $scope.$apply();
          }
        }
      };

      $scope.destroyDefaultValue = function(event){
        if(event){
          event.stopPropagation();
        }
        setSelect();
        $scope.ngModel = undefined;

        if($scope.ngChange){
          $timeout(function(){
            $scope.ngChange();
          }, 0);
        }
      };

      $document.bind('click', function() {
        closeList();
      });

      $scope.setNewValue = function(item, event){
        if(event){event.stopPropagation();}
        if(!item){return closeList();}

        if(item){
          ngModelWatcher(); // unbind if value is set
          setSelect(item);
          $scope.ngModel = item;
          input.focus();
          if($scope.ngChange){
            $timeout(function(){
              $scope.ngChange();
            }, 0);
          }
        }
      };

      $scope.getContent = function(item){
        if(item){
          if(angular.isFunction(options.list)){
            return options.list(item);
          }else{
            return item[options.list];
          }
        }else{
          return 'Keine Einträge'; // TODO: Übersetzung Selectmenü wenn keine Einträge hardcoded
        }
      };

      var setSelect = function(item){
        if(item){
          if(angular.isFunction(options.select)){
            $scope.select = options.select(item);
          }else{
            $scope.select = item[options.select];
          }
        }else{
          $scope.select = undefined;
        }
      };

      /**
       * Auswahl setzen, falls vorhanden
       * @type {*|(function())}
       */
      var ngModelWatcher = $scope.$watch('ngModel', function(val) {
        if(val !== undefined){
          ngModelWatcher(); // unbind if value is set
          if(val){
            setSelect(val);
          }else{
            $scope.items = [];
          }
        }
      });
    }
  };
});

apselect.directive('combo2remote', function($parse, $log, $document, $http) {
  return {
    restrict: 'E',
    scope: {
      record: '=',
      remoteConfig: '=',
      name: '@'
    },
    templateUrl: 'bower_components/apselect/templates/combo2remote.html',
    replace: true,
    transclude: true,
    link: function($scope, element){
      var init = false;
      $scope.items = [];

      // CONFIG
      var options = {
        table: 'tests',
        id: '_id',
        list: 'text',
        select: 'text',
        fields: []
      };


      var setList = function(data){
        $scope.selected = 0;
        $scope.items = data;
        element.addClass('open');

        element.unbind('keydown');
        element.on('keydown', function(e) {
          if (e.which === 13) {
            $scope.setNewValue($scope.items[$scope.selected]);
            $scope.$apply();
          }else if(e.which === 38){
            if($scope.selected > 0){
              $scope.selected--;
              $scope.$apply();
            }
          }else if(e.which === 40){
            if(($scope.selected + 1) < $scope.items.length){
              $scope.selected++;
              $scope.$apply();
            }
          }
        });

      };

      var closeList = function(){
        element.unbind('keydown');
        $scope.items = [];
        element.removeClass('open');
      };

      $scope.setSelected = function(index){
        $scope.selected = index;
      };


      angular.extend(options, $scope.remoteConfig);

      $scope.$watch('record', function(newValue){
        if(newValue){
          if(newValue !== undefined){
            if(init){
              $http.post('/search/select', {table: options.table, q: newValue, id: '', fields: options.fields}).then(function (res) {
                if(res.data.length){
                  setList(res.data);
                }else{
                  closeList();
                }
              });
            }else{
              init = true;
              closeList();
            }
          }else{
            closeList();
          }
        }
      });

      $document.bind('click', function() {
        element.removeClass('open');
      });

      $scope.setNewValue = function(item){
        if(item){
          if(angular.isFunction(options.select)){
            options.select(item);
            closeList();
            init = false;
          }else{
            closeList();
          }
        }
      };

      $scope.getContent = function(item){
        if(angular.isFunction(options.list)){
          return options.list(item);
        }else{
          return item[options.list];
        }
      };

    }
  };
});

apselect.getRecursiveProperty = function (object, path) {
  return path.split('.').reduce(function (object, x) {
    if (object) {
      return object[x];
    } else {
      return null;
    }
  }, object)
};

apselect.directive('multiselect', function ($filter, $document, $log) {
  return {
    restrict: 'AE',
    scope: {
      options: '=',
      displayProp: '@',
      idProp: '@',
      searchLimit: '=?',
      selectionLimit: '=?',
      showSelectAll: '=?',
      showUnselectAll: '=?',
      showSearch: '=?',
      searchFilter: '=?',
      disabled: '=?ngDisabled',
      defaultText: '@'
    },
    require: 'ngModel',
    templateUrl: 'bower_components/apselect/templates/multiselect.html',
    link: function ($scope, $element, $attrs, $ngModelCtrl) {
      $scope.selectionLimit = $scope.selectionLimit || 0;
      $scope.searchLimit = $scope.searchLimit || 10;
      $scope.defaultText = $scope.defaultText || 'Auswählen';

      $scope.searchFilter = '';

      $scope.resolvedOptions = [];
      if (typeof $scope.options !== 'function') {
        $scope.resolvedOptions = $scope.options;
      }

      if (typeof $attrs.disabled != 'undefined') {
        $scope.disabled = true;
      }

      $scope.toggleDropdown = function () {
        $scope.open = !$scope.open;
      };

      var closeHandler = function (event) {
        if (!$element[0].contains(event.target)) {
          $scope.$apply(function () {
            $scope.open = false;
          });
        }
      };

      $document.on('click', closeHandler);

      var updateSelectionLists = function () {
        if (!$ngModelCtrl.$viewValue) {
          if ($scope.selectedOptions) {
            $scope.selectedOptions = [];
          }
          $scope.unselectedOptions = $scope.resolvedOptions.slice(); // Take a copy
        } else {
          if ($scope.unselectedOptions && $scope.unselectedOptions.length === 0) { $scope.open = false; } // Keine Einträge mehr zum auswählen

          $scope.selectedOptions = $scope.resolvedOptions.filter(function (el) {
            var id = $scope.getId(el);
            for (var i = 0; i < $ngModelCtrl.$viewValue.length; i++) {
              var selectedId = $scope.getId($ngModelCtrl.$viewValue[i]);
              if (id === selectedId) {
                return true;
              }
            }
            return false;
          });
          $scope.unselectedOptions = $scope.resolvedOptions.filter(function (el) {
            return $scope.selectedOptions.indexOf(el) < 0;
          });
        }
      };

      $ngModelCtrl.$render = function () {
        updateSelectionLists();
      };

      $ngModelCtrl.$viewChangeListeners.push(function () {
        updateSelectionLists();
      });

      $ngModelCtrl.$isEmpty = function (value) {
        if (value) {
          return (value.length === 0);
        } else {
          return true;
        }
      };

      var watcher = $scope.$watch('selectedOptions', function () {
        $ngModelCtrl.$setViewValue(angular.copy($scope.selectedOptions));
      }, true);

      $scope.$on('$destroy', function () {
        $document.off('click', closeHandler);
        if (watcher) {
          watcher(); // Clean watcher
        }
      });

      $scope.getButtonText = function () {
        if ($scope.selectedOptions && $scope.selectedOptions.length === 1) {
          return $scope.getDisplay($scope.selectedOptions[0]);
        }
        if ($scope.selectedOptions && $scope.selectedOptions.length > 1) {
          var totalSelected;
          totalSelected = angular.isDefined($scope.selectedOptions) ? $scope.selectedOptions.length : 0;
          if (totalSelected === 0) {
            return $scope.defaultText;
          } else {
            return totalSelected + ' ' + 'ausgewählt';
          }
        } else {
          return $scope.defaultText;
        }
      };

      $scope.selectAll = function () {
        $scope.selectedOptions = $scope.resolvedOptions;
        $scope.unselectedOptions = [];
      };

      $scope.unselectAll = function () {
        $scope.selectedOptions = [];
        $scope.unselectedOptions = $scope.resolvedOptions;
      };

      $scope.toggleItem = function (item) {
        if (typeof $scope.selectedOptions === 'undefined') {
          $scope.selectedOptions = [];
        }
        var selectedIndex = $scope.selectedOptions.indexOf(item);
        var currentlySelected = (selectedIndex !== -1);
        if (currentlySelected) {
          $scope.unselectedOptions.push($scope.selectedOptions[selectedIndex]);
          $scope.selectedOptions.splice(selectedIndex, 1);
        } else if (!currentlySelected && ($scope.selectionLimit === 0 || $scope.selectedOptions.length < $scope.selectionLimit)) {
          var unselectedIndex = $scope.unselectedOptions.indexOf(item);
          $scope.unselectedOptions.splice(unselectedIndex, 1);
          $scope.selectedOptions.push(item);
        }
      };

      $scope.getId = function (item) {
        if (angular.isString(item)) {
          return item;
        } else if (angular.isObject(item)) {
          if ($scope.idProp) {
            return apselect.getRecursiveProperty(item, $scope.idProp);
          } else {
            $log.error('Multiselect: when using objects as model, a idProp value is mandatory.');
            return '';
          }
        } else {
          return item;
        }
      };

      $scope.getDisplay = function (item) {
        if (angular.isString(item)) {
          return item;
        } else if (angular.isObject(item)) {
          if ($scope.displayProp) {
            return apselect.getRecursiveProperty(item, $scope.displayProp);
          } else {
            $log.error('Multiselect: when using objects as model, a displayProp value is mandatory.');
            return '';
          }
        } else {
          return item;
        }
      };

      $scope.isSelected = function (item) {
        if (!$scope.selectedOptions) {
          return false;
        }
        var itemId = $scope.getId(item);
        for (var i = 0; i < $scope.selectedOptions.length; i++) {
          var selectedElement = $scope.selectedOptions[i];
          if ($scope.getId(selectedElement) === itemId) {
            return true;
          }
        }
        return false;
      };

      $scope.updateOptions = function () {
        if (typeof $scope.options === 'function') {
          $scope.options().then(function (resolvedOptions) {
            $scope.resolvedOptions = resolvedOptions;
            updateSelectionLists();
          });
        }
      };

      // This search function is optimized to take into account the search limit.
      // Using angular limitTo filter is not efficient for big lists, because it still runs the search for
      // all elements, even if the limit is reached
      $scope.search = function () {
        var counter = 0;
        return function (item) {
          if (counter > $scope.searchLimit) {
            return false;
          }
          var displayName = $scope.getDisplay(item);
          if (displayName) {
            var result = displayName.toLowerCase().indexOf($scope.searchFilter.toLowerCase()) > -1;
            if (result) {
              counter++;
            }
            return result;
          }
        }
      };
    }
  };
});

apselect.directive('multiselectList', function ($timeout) {
  return {
    restrict: 'AE',
    scope: {
      options: '=',
      displayProp: '@',
      idProp: '@',
      ngModel: '=',
      ngChange: '&'
    },
    templateUrl: 'bower_components/apselect/templates/multiselect-list.html',
    link: function ($scope, $element, $attrs) {

      var change = function(){
        if($scope.ngChange){
          $timeout(function(){
            $scope.ngChange();
          }, 0);
        }
      }

      $scope.toggle = function(id){
        $scope.ngModel[id] = !$scope.ngModel[id]
        change();
      };

      $scope.selectAll = function(value){
        for(var i = 0; i < $scope.options.length; i++){
          $scope.ngModel[$scope.options[i][$scope.idProp]] = value;
        }
        change();
      };
    }
  };
});
